// Name: WT Game Framework
// Created by: romw314 <https://github.com/romw314>

const c = 'romwthreeonefour';
const i = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAxpJREFUeF7tmztSwzAQhuWOlhswQEdLxR1gJpyAK6ShSpmKJlfgBpmBO1DRUobhAgwtnRkrUZBlyf+u1vL4IRpI9Nr99O/qYVOUSpXK+imUKuzP3L+T97epmcs1T6llUfOvSG5w10ClACpkFoR5ArAgzBfAAcIxHqSh4LZ3g5ObW4L2bMp6EnBi2psU3Db1nLf/NGkAlYMBCPNQgJlxD4R5AfAoobHmc0MB1UflbszC+jE5gKIAUwca4FiM6qPy3gCgHDBaAPy9YK3F+ENAAmBZ1PfFdl9IuqicK21yfy1rOovFYf8QPPggg1D5oAHYZ4EQNeQgKu8NAGUn2CKN8SsgFYDQqtD1Hp/dn2Qf4FECvPxIfcjJABwC8NSYFRBxHI5JgtwszlqDJcfvvhUQu0VGQLjL6LG/DCCHAP9KrIscgCQ91nK4DxirY1S7MwAqqanWywqY6sxS/coKoJKaaj22Al6c9wlsMHfCR+FUyF3awAJQrlT5ug6bebtSqljLXrBAELq2gQSgGtQYhgCYel2DSGVDKwB7UDQzvvKuIKS0wwtAOmDjmisyLPqwQwSgmmGqkVw1cPrl1PXcQTbFizoMOYPaxYQRJ7TQ+D67WQqgzCIyQgpBYoMIAGXg4/WZtWpIHbbbS20gA6gGdWeSM7ivvRSEdPxQe3IIcA2QOixt7wtFsgIojRfPO7052j5ckDZTUoeC7c0tsfOMkOJD1We0AgYBoOWKPDmA43t3wqezImX0BaAROx09nDh5fNdh9Pt0HR9GgRCgJvLmO0KeJawGoBrQnXXfd2BqjfOmGhlCi8O6L6ucEgYkAPeXO9339ud8b68PgFJqcfq5rwcSo3H+5upM13/7+NK/IQSkPKe8/G4Ky1VzBuAqFcpmbiHg3QAhKRJTe+okSEmEtRCAs287hpIREYKoGmEikE/RAIa+ETJgyQBCx9jQGWAQAJxlz6cm5Nf//wsEjrBjOwRRkro+Axyu6VoBjN15ShhoAEgmokQ1gMZt/gUBTGX2kQqCt7pzAKBzQepLzAFEQKsJGUBWQKIr7KFL39j3B5FbMJwLeVwcAAAAAElFTkSuQmCC';

class WTGameExtension {
	#timers = {
		main: -1,
		secondary: -1,
		timeout: -1
	};

	#health = -1;

	getInfo() {
		return {
			id: `${c}wtgameframework`,
			name: Scratch.translate('WP Game'),
			color1: '#8ec410',
			color2: '#6c9311',
			color3: '#506816',
			menuIconURI: i,
			blockIconURI: i,
			blocks: [
				{
					opcode: 'resetMainTimer',
					hideFromPalette: true,
					blockType: Scratch.BlockType.COMMAND,
					text: Scratch.translate('start/reset main game timer'),
					arguments: {}
				},
				{
					opcode: 'mainGameTimer',
					hideFromPalette: true,
					blockType: Scratch.BlockType.REPORTER,
					text: Scratch.translate('main game timer'),
					arguments: {}
				},
				{
					opcode: 'stopMainTimer',
					hideFromPalette: true,
					blockType: Scratch.BlockType.COMMAND,
					text: Scratch.translate('stop main game timer'),
					arguments: {}
				},
				{
					opcode: 'resetTimer',
					blockType: Scratch.BlockType.COMMAND,
					text: Scratch.translate('start/reset [TIMER] game timer'),
					arguments: {
						TIMER: {
							type: Scratch.ArgumentType.STRING,
							menu: 'TIMER_MENU'
						}
					}
				},
				{
					opcode: 'resetTimeoutTimer',
					blockType: Scratch.BlockType.COMMAND,
					text: Scratch.translate('start [TIMER] game timer for [TIMEOUT] seconds'),
					arguments: {
						TIMER: {
							type: Scratch.ArgumentType.STRING,
							menu: 'TIMEOUT_TIMER_MENU'
						},
						TIMEOUT: {
							type: Scratch.ArgumentType.NUMBER,
							defaultValue: '10'
						}
					}
				},
				{
					opcode: 'gameTimer',
					blockType: Scratch.BlockType.REPORTER,
					text: Scratch.translate('[TIMER] game timer'),
					arguments: {
						TIMER: {
							type: Scratch.ArgumentType.STRING,
							menu: 'ANY_TIMER_MENU'
						}
					}
				},
				{
					opcode: 'runningTimer',
					blockType: Scratch.BlockType.BOOLEAN,
					text: Scratch.translate('[TIMER] game timer is running'),
					arguments: {
						TIMER: {
							type: Scratch.ArgumentType.STRING,
							menu: 'ANY_TIMER_MENU'
						}
					}
				},
				{
					opcode: 'stopTimer',
					blockType: Scratch.BlockType.COMMAND,
					text: Scratch.translate('stop [TIMER] game timer'),
					arguments: {
						TIMER: {
							type: Scratch.ArgumentType.STRING,
							menu: 'ANY_TIMER_MENU'
						}
					}
				},
				{
					opcode: 'whenTimerReaches',
					blockType: Scratch.BlockType.HAT,
					text: Scratch.translate('when [TIMER] game timer > [TIME]'),
					isEdgeActivated: true,
					arguments: {
						TIMER: {
							type: Scratch.ArgumentType.STRING,
							menu: 'TIMER_MENU'
						},
						TIME: {
							type: Scratch.ArgumentType.NUMBER,
							defaultValue: '3'
						}
					}
				},
				{
					opcode: 'whenTimeoutTimerTimesOut',
					blockType: Scratch.BlockType.HAT,
					text: Scratch.translate('when [TIMER] game timer times out'),
					isEdgeActivated: true,
					arguments: {
						TIMER: {
							type: Scratch.ArgumentType.STRING,
							menu: 'TIMEOUT_TIMER_MENU'
						}
					}
				},
				'---',
				{
					opcode: 'setHealth',
					blockType: Scratch.BlockType.COMMAND,
					text: Scratch.translate('set health to [HEALTH]'),
					arguments: {
						HEALTH: {
							type: Scratch.ArgumentType.NUMBER,
							defaultValue: '100'
						}
					}
				},
				{
					opcode: 'getHealth',
					blockType: Scratch.BlockType.REPORTER,
					text: Scratch.translate('health'),
					arguments: {}
				},
				{
					opcode: 'onDie',
					blockType: Scratch.BlockType.HAT,
					text: Scratch.translate('on die'),
					isEdgeActivated: true,
					arguments: {}
				},
				{
					opcode: 'damage',
					blockType: Scratch.BlockType.COMMAND,
					text: Scratch.translate('damage by [DAMAGE] health points'),
					arguments: {
						DAMAGE: {
							type: Scratch.ArgumentType.NUMBER,
							defaultValue: '10'
						}
					}
				},
				{
					opcode: 'gainHealth',
					blockType: Scratch.BlockType.COMMAND,
					text: Scratch.translate('gain [HEALTH] health points'),
					arguments: {
						HEALTH: {
							type: Scratch.ArgumentType.NUMBER,
							defaultValue: '10'
						}
					}
				}
			],
			menus: {
				TIMER_MENU: {
					acceptReporters: true,
					items: ['main', 'secondary']
				},
				ANY_TIMER_MENU: {
					acceptReporters: true,
					items: ['main', 'secondary', 'timeout']
				},
				TIMEOUT_TIMER_MENU: {
					acceptReporters: true,
					items: ['timeout']
				}
			}
		};
	}

	whenTimerReaches({ TIMER, TIME }) {
		return this.gameTimer(TIMER) > Scratch.Cast.toNumber(TIME);
	}
	whenTimeoutTimerTimesOut({ TIMER }) {
		if (this.runningTimer({ TIMER }) && this.gameTimer({ TIMER }) <= 0) {
			this.stopTimer({ TIMER });
			return true;
		}
		return false;
	}
	mainGameTimer() {
		this.gameTimer({ TIMER: 'main' });
	}
	resetMainTimer() {
		this.resetTimer({ TIMER: 'main' });
	}
	stopMainTimer() {
		this.stopTimer({ TIMER: 'main' });
	}
	gameTimer({ TIMER }) {
		const time = this.#timers[TIMER];
		if (time === -1)
			return 0;
		if (TIMER === 'timeout')
			return (time - Date.now()) / 1000;
		return (Date.now() - time) / 1000;
	}
	runningTimer({ TIMER }) {
		return this.#timers[TIMER] >= 0;
	}
	resetTimer({ TIMER }) {
		this.#timers[TIMER] = Date.now();
	}
	resetTimeoutTimer({ TIMER, TIMEOUT }) {
		this.#timers[TIMER] = Date.now() + (Scratch.Cast.toNumber(TIMEOUT) * 1000);
	}
	stopTimer({ TIMER }) {
		this.#timers[TIMER] = -1;
	}

	setHealth({ HEALTH }) {
		this.#health = Scratch.Cast.toNumber(HEALTH);
	}
	getHealth() {
		return this.#health;
	}
	onDie() {
		return this.#health <= 0;		
	}
	damage({ DAMAGE }) {
		this.#health -= Scratch.Cast.toNumber(DAMAGE);
	}
	gainHealth({ HEALTH }) {
		this.#health += Scratch.Cast.toNumber(HEALTH);
	}
}
Scratch.extensions.register(new WTGameExtension());
