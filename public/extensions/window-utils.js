(function(Scratch){
	const c = 'romwthreeonefour';

	function makeLabel(text) {
		return { blockType: 'label', text };
	}

	class WindowUtilsExtension {
		getInfo() {
			return {
				id: `${c}windowutils`,
				name: 'Window Utilities',
				color1: '#b12124',
				color2: '#991010',
				color3: '#510505',
				blocks: [
					{
						opcode: 'openWebsite',
						blockType: Scratch.BlockType.COMMAND,
						text: 'open [URL] in new tab',
						arguments: {
							URL: {
								type: Scratch.ArgumentType.STRING,
								defaultValue: 'https://extensions.turbowarp.org/hello.html'
							}
						}
					},
					{
						opcode: 'openHTML',
						blockType: Scratch.BlockType.COMMAND,
						text: 'open HTML [HTML] in new tab',
						arguments: {
							HTML: {
								type: Scratch.ArgumentType.STRING,
								defaultValue: '<h1>Hello world!</h1>'
							}
						}
					},
					{
						opcode: 'setPath',
						blockType: Scratch.BlockType.COMMAND,
						text: 'change URL path to [PATH]',
						arguments: {
							PATH: {
								type: Scratch.ArgumentType.STRING,
								defaultValue: '/my/url/path'
							}
						}
					},
					{
						opcode: 'navigate',
						blockType: Scratch.BlockType.COMMAND,
						text: 'navigate to [URL]',
						arguments: {
							URL: {
								type: Scratch.ArgumentType.STRING,
								defaultValue: 'https://romw314.github.io'
							}
						}
					},
					{
						opcode: 'reload',
						blockType: Scratch.BlockType.COMMAND,
						text: 'reload site',
						arguments: {}
					},
					{
						opcode: 'changeHTML',
						blockType: Scratch.BlockType.COMMAND,
						text: 'set HTML to [HTML]',
						arguments: {
							HTML: {
								type: Scratch.ArgumentType.STRING,
								menu: 'HTMLM_A'
							}
						}
					},
					{
						opcode: 'getRaw',
						blockType: Scratch.BlockType.REPORTER,
						text: 'HTML [DATA]',
						arguments: {
							DATA: {
								type: Scratch.ArgumentType.STRING,
								defaultValue: '<h1>Hello, World</h1>'
							}
						}
					}
				],
				menus: {
					HTMLM_A: {
						acceptReporters: true,
						items: [
							'<button onclick="document.body.innerHTML+=\'<p>Hey! You clicked the button!</p>\';">Click me!</button>',
							'<h1>Hello, World!</h1>',
							'<button>Uh! Dead button!</button>',
							'<input id="exampleinput" oninput="document.getElementById(\'valtext\').innerText=document.getElementById(\'exampleinput\').value;" value="Hello, World!"/><p id="valtext">Hello, World!</p>'
						]
					}
				}
			};
		}
		openWebsite({ URL }) {
			window.open(URL.toString());
		}
		openHTML({ HTML }) {
			const win = window.open();
			win.document.body.innerHTML = HTML;
		}
		setPath({ PATH }) {
			window.history.pushState({}, '', PATH.toString());
		}
		navigate({ URL }) {
			window.location.href = URL;
		}
		reload() {
			window.location.reload();
		}
		changeHTML({ HTML }) {
			document.body.innerHTML = HTML;
		}
		getRaw({ DATA }) {
			return DATA;
		}
	}
	Scratch.extensions.register(new WindowUtilsExtension());
})(Scratch);
