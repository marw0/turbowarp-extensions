const c = 'romwthreeonefour';

class HashExtension {
	getInfo() {
		return {
			id: `${c}hash`,
			name: Scratch.translate('Hash & Encode'),
			blocks: [
				{
					opcode: 'encodeToNumbers',
					blockType: Scratch.BlockType.REPORTER,
					text: Scratch.translate('encode [DATA] to numbers'),
					arguments: {
						DATA: {
							type: Scratch.ArgumentType.STRING,
							defaultValue: 'apple'
						}
					}
				},
				{
					opcode: 'decodeFromNumbers',
					blockType: Scratch.BlockType.REPORTER,
					text: Scratch.translate('decode [DATA] from numbers'),
					arguments: {
						DATA: {
							type: Scratch.ArgumentType.NUMBER,
							defaultValue: '1789365700828261515365'
						}
					}
				},
				'---',
				{
					opcode: 'hash',
					blockType: Scratch.BlockType.REPORTER,
					text: Scratch.translate('hash [DATA] with [TYPE]'),
					arguments: {
						DATA: {
							type: Scratch.ArgumentType.STRING,
							defaultValue: 'apple'
						},
						TYPE: {
							type: Scratch.ArgumentType.STRING,
							menu: 'HASH_MENU',
							defaultValue: 'SHA-256'
						}
					}
				},
				'---',
				{
					opcode: 'encryptData',
					blockType: Scratch.BlockType.REPORTER,
					text: Scratch.translate('encrypt [DATA] with [PASSWORD]'),
					arguments: {
						DATA: {
							type: Scratch.ArgumentType.STRING,
							defaultValue: 'apple'
						},
						PASSWORD: {
							type: Scratch.ArgumentType.STRING,
							defaultValue: 'my secret password'
						}
					}
				},
				{
					opcode: 'decryptData',
					blockType: Scratch.BlockType.REPORTER,
					text: Scratch.translate('decrypt [DATA] with [PASSWORD]'),
					arguments: {
						DATA: {
							type: Scratch.ArgumentType.STRING,
							defaultValue: 'apple'
						},
						PASSWORD: {
							type: Scratch.ArgumentType.STRING,
							defaultValue: 'my secret password'
						}
					}
				}
			],
			menus: {
				HASH_MENU: {
					acceptReporters: true,
					items: ['SHA-256', 'SHA-512']
				}
			}
		};
	}

	encodeToNumbers({ DATA }) {
		const s = String(DATA);
		let result = '0b';
		for (let i = 0; i < s.length; i++) {
			const newStr = s.charCodeAt(i).toString(2).padStart(16, '0');
			console.log('[HashEncode] add', newStr);
			result += newStr;
			console.log('[HashEncode] numEncode', s.charCodeAt(i));
		}
		return BigInt(result).toString();
	}

	decodeFromNumbers({ DATA }) {
		let s = BigInt(DATA.toString()).toString(2);
		s = s.padStart(Math.ceil(s.length / 16) * 16, '0');
		console.log('[HashEncode] numDecode', s);
		let result = '';
		for (let i = 0; i < s.length; i+=16) {
			result += String.fromCharCode(parseInt(s.substr(i,16),2));
		}
		return result;
	}

	hash({ DATA, TYPE }) {
		switch (TYPE) {
			case 'SHA-256':
			case 'SHA-512':
				return this.#cryptoHash(DATA, TYPE);
		}
	}
	async #cryptoHash(data, hashName) {
		const textAsBuffer = new TextEncoder().encode(data);
		const hashBuffer = await window.crypto.subtle.digest(hashName, textAsBuffer);
		const hashArray = Array.from(new Uint8Array(hashBuffer))
		const digest = hashArray.map(b => b.toString(16).padStart(2, '0')).join('');
		return digest;
	}

	// Thanks to the working solution from stack overflow for these three methods, adapted to work with this extension:
	// https://stackoverflow.com/questions/62102034/javascript-how-to-encrypt-string-with-only-password-in-2020
	async #deriveKey(password) {
		const algo = {
			name: 'PBKDF2',
			hash: 'SHA-256',
			salt: new TextEncoder().encode('a-unique-salt-for-that'),
			iterations: 1000
		};
		return crypto.subtle.deriveKey(
			algo,
			await crypto.subtle.importKey(
				'raw',
				new TextEncoder().encode(password), {
					name: algo.name
				},
				false,
				['deriveKey']
			), {
				name: 'AES-GCM',
				length: 256
			},
			false,
			['encrypt', 'decrypt']
		);
	}
	async #encrypt(text, password) {
		const algo = {
			name: 'AES-GCM',
			length: 256,
			iv: crypto.getRandomValues(new Uint8Array(12))
		};
		return {
			cipherText: await crypto.subtle.encrypt(
				algo,
				await this.#deriveKey(password),
				new TextEncoder().encode(text)
			),
			iv: algo.iv
		};
	}
	async #decrypt(encrypted, password) {
		const algo = {
			name: 'AES-GCM',
			length: 256,
			iv: encrypted.iv
		};
		return new TextDecoder().decode(
			await crypto.subtle.decrypt(
				algo,
				await this.#deriveKey(password),
				encrypted.cipherText
			)
		);
	}

	#ab2str(buf) {
		return String.fromCharCode.apply(null, new Uint8Array(buf));
	}
	#str2ab(str) {
		var buf = new ArrayBuffer(str.length);
		var bufView = new Uint8Array(buf);
		for (var i=0, strLen=str.length; i<strLen; i++) {
			bufView[i] = str.charCodeAt(i);
		}
		return buf;
	}

	decryptData({ DATA, PASSWORD }) {
		const parsed = JSON.parse(DATA);
		return this.#decrypt({ cipherText: this.#str2ab(parsed[0]), iv: new Uint8Array(parsed.slice(1)) }, PASSWORD);
	}
	encryptData({ DATA, PASSWORD }) {
		return this.#encrypt(DATA, PASSWORD).then(({ cipherText, iv }) => JSON.stringify([this.#ab2str(cipherText), ...iv]));
	}
}
Scratch.extensions.register(new HashExtension());
