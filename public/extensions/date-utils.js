const c = 'romwthreeonefour';

class DateUtilsExtension {
	getInfo() {
		return {
			id: `${c}dateutils`,
			name: Scratch.translate('Date Utilities'),
			blocks: [
				{
					opcode: 'daysOfMonth',
					blockType: Scratch.BlockType.REPORTER,
					text: Scratch.translate('number of days of month [MONTH] in [YEAR]'),
					arguments: {
						MONTH: {
							type: Scratch.ArgumentType.NUMBER,
							defaultValue: '2'
						},
						YEAR: {
							type: Scratch.ArgumentType.NUMBER,
							defaultValue: '2020'
						}
					}
				},
				{
					opcode: 'dateToUTC',
					hideFromPalette: true,
					blockType: Scratch.BlockType.REPORTER,
					text: Scratch.translate('convert [DATE] to UTC'),
					arguments: {
						DATE: {
							type: Scratch.ArgumentType.STRING,
							defaultValue: 'Thu Dec 14 2023 20:17:23 GMT+0100 (Central European Standard Time)'
						}
					}
				},
				{
					opcode: 'dateToLocal',
					hideFromPalette: true,
					blockType: Scratch.BlockType.REPORTER,
					text: Scratch.translate('convert [DATE] to local time'),
					arguments: {
						DATE: {
							type: Scratch.ArgumentType.STRING,
							defaultValue: 'Thu, 14 Dec 2023 19:17:23 GMT'
						}
					}
				},
				{
					opcode: 'convertDate',
					blockType: Scratch.BlockType.REPORTER,
					text: Scratch.translate('convert [DATE] to [FORMAT]'),
					arguments: {
						DATE: {
							type: Scratch.ArgumentType.STRING,
							defaultValue: '1702581443000'
						},
						FORMAT: {
							type: Scratch.ArgumentType.STRING,
							menu: 'FORMAT_MENU',
							defaultValue: 'local time'
						}
					}
				},
				{
					opcode: 'timestamp',
					hideFromPalette: true,
					blockType: Scratch.BlockType.REPORTER,
					text: Scratch.translate('current timestamp'),
					arguments: {}
				},
				{
					opcode: 'currentDate',
					blockType: Scratch.BlockType.REPORTER,
					text: Scratch.translate('current [FORMAT]'),
					arguments: {
						FORMAT: {
							type: Scratch.ArgumentType.STRING,
							menu: 'FORMAT_MENU',
							defaultValue: 'timestamp'
						}
					}
				},
				{
					opcode: 'getFromDate',
					blockType: Scratch.BlockType.REPORTER,
					text: Scratch.translate('get [FIELD] from [DATE] in [ZONE]'),
					arguments: {
						FIELD: {
							type: Scratch.ArgumentType.STRING,
							menu: 'FIELD_MENU',
							defaultValue: 'year'
						},
						DATE: {
							type: Scratch.ArgumentType.STRING,
							defaultValue: '1702581443000'
						},
						ZONE: {
							type: Scratch.ArgumentType.STRING,
							menu: 'TIMEZONE_MENU',
							defaultValue: 'local time'
						}
					}
				}
			],
			menus: {
				FORMAT_MENU: {
					acceptReporters: true,
					items: ['timestamp', 'local time', 'UTC', 'ISO']
				},
				TIMEZONE_MENU: {
					acceptReporters: true,
					items: ['local time', 'UTC']
				},
				FIELD_MENU: {
					acceptReporters: true,
					items: ['year', 'month', 'day', 'day of week', 'hour', 'minute', 'second', 'millisecond']
				}
			}
		};
	}
	daysOfMonth({ MONTH, YEAR }) {
		return new Date(YEAR, MONTH, 0).getDate();
	}
	dateToUTC({ DATE }) {
		return this.convertDate({ DATE, FORMAT: 'UTC' });
	}
	dateToLocal({ DATE }) {
		return this.convertDate({ DATE, FORMAT: 'local time' });
	}
	convertDate({ DATE, FORMAT }) {
		if (FORMAT === 'timestamp')
			return Number(new Date(DATE));
		else if (FORMAT === 'local time')
			return new Date(DATE).toString();
		else if (FORMAT === 'UTC')
			return new Date(DATE).toUTCString();
		else if (FORMAT === 'ISO')
			return new Date(DATE).toISOString();
	}
	timestamp() {
		return Date.now();
	}
	currentDate({ FORMAT }) {
		return this.convertDate({ DATE: Date.now(), FORMAT });
	}
	getFromDate({ DATE, FIELD, ZONE }) {
		const date = new Date(DATE);
		switch (ZONE) {
			case 'local time':
				switch (FIELD) {
					case 'year': return date.getFullYear();
					case 'month': return date.getMonth();
					case 'day': return date.getDate();
					case 'day of week': return date.getDay();
					case 'hour': return date.getHours();
					case 'minute': return date.getMinutes();
					case 'second': return date.getSeconds();
					case 'millisecond': return date.getMilliseconds();
				}
				break;
			case 'UTC':
				switch (FIELD) {
					case 'year': return date.getUTCFullYear();
					case 'month': return date.getUTCMonth();
					case 'day': return date.getUTCDate();
					case 'day of week': return date.getUTCDay();
					case 'hour': return date.getUTCHours();
					case 'minute': return date.getUTCMinutes();
					case 'second': return date.getUTCSeconds();
					case 'millisecond': return date.getUTCMilliseconds();
				}
				break;
		}
	}
}
Scratch.extensions.register(new DateUtilsExtension());
