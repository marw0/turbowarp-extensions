# Contributing

## Creating extensions

If you want to add your own extension, you can. Place your own extensions under the `public/extensions/community` directory and submit a merge request. The extension id shouldn't begin with `romw314` or `romwthreeonefour` or `romwthreehundredtenfour`. If you have time, please create an example that uses as much as you can blocks of the extension and describes what is the program doing, what are the blocks used for and why in comments. Place the examples under the `public/examples/community` directory.

### Naming

The extension file should have a short name separated by hyphens (`-`) and have the `.js` extension, for example, `date-utils.js`.

The example file should have the exactly same name as the full name of the extension.

## Requesting extensions

If you want to request an extension, feel free to open an issue and I will look at it.
