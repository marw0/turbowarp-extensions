## Name

(name of the extension)

## Description

(what is the extension for)

## How to use

(how to use the extension)

## IMPORTANT

By submitting this merge request:

* I confirm that the extension in not in violation with the [contributing guidelines](https://gitlab.com/romw314/turbowarp-extensions/-/blob/master/CONTRIBUTING.md)
* I confirm that the extension can be integrated into this repository and licensed under [The Unlicense](https://gitlab.com/romw314/turbowarp-extensions/-/blob/master/LICENSE)
* I confirm that neither the extension nor the example contains any copyright or license
* I am aware that the extension may be updated by anyone and don't need my explicit content so
